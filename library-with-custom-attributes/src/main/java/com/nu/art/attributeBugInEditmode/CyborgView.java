package com.nu.art.attributeBugInEditmode;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.TypedArray;
import android.os.Build.VERSION_CODES;
import android.util.AttributeSet;
import android.widget.TextView;

import test.sensiya.art.nu.com.librarywithcustomattributes.R;

/**
 * Created by TacB0sS on 28-Apr 2016.
 */
public class CyborgView
		extends TextView {

	public CyborgView(Context context) {
		super(context);
		init(null);
	}

	public CyborgView(Context context, AttributeSet attrs) {
		super(context, attrs);
		init(attrs);
	}

	public CyborgView(Context context, AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		init(attrs);
	}

	@TargetApi(VERSION_CODES.LOLLIPOP)
	public CyborgView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
		super(context, attrs, defStyleAttr, defStyleRes);
	}

	private void init(AttributeSet attrs) {
		TypedArray a = getContext().obtainStyledAttributes(attrs, R.styleable.CyborgView);
		String controllerName = a.getString(R.styleable.CyborgView_controller);
		if (controllerName == null)
			throw new RuntimeException("No controller name found");
		a.recycle();

		setText(controllerName);
	}
}
